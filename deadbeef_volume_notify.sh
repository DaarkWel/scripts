#!/bin/bash
# created by abarilla
# changed for deadbeef by daarkwel

# Volume bar in notification message window.

# Unlicenced, I guess.

#set -x

usage="usage: $0 -c {up|down|mute} [-i increment]"
command=
increment=5

while getopts i:m:h o
do case "$o" in
    i) increment=$OPTARG;;
    h) echo "$usage"; exit 0;;
    ?) echo "$usage"; exit 0;;
esac
done

shift $(($OPTIND - 1))
command=$1

if [ "$command" = "" ]; then
    echo "usage: $0 {up|down|mute} [increment]"
    exit 0;
fi

display_volume=0

if [ "$command" = "up" ]; then
    display_volume=$(deadbeef --volume +$increment 2>/dev/null | cut -d "%" -f1)
fi

if [ "$command" = "down" ]; then
    display_volume=$(deadbeef --volume -$increment 2>/dev/null | cut -d "%" -f1)
fi

icon_name=""

if [ "$command" = "mute" ]; then
    if [[ $(deadbeef --volume 2>/dev/null | cut -d "%" -f1) -ne 0 ]]; then
    	printf "%s" "$(deadbeef --volume 2>/dev/null | cut -d "%" -f1)" > "/tmp/deadbeef_volume_mem"
        display_volume=0
        icon_name="$HOME/.icons/audio-volume-muted.svg"
        deadbeef --volume 0
    else
        display_volume=$(deadbeef --volume "$(</tmp/deadbeef_volume_mem)" 2>/dev/null | cut -d "%" -f1)
    fi
fi

if [ "$icon_name" = "" ]; then
    if [ "$display_volume" = "0" ]; then
        icon_name="$HOME/.icons/audio-volume-muted.svg"
    else
        if [ "$display_volume" -lt "33" ]; then
            icon_name="$HOME/.icons/audio-volume-low.svg"
        else
            if [ "$display_volume" -lt "67" ]; then
                icon_name="$HOME/.icons/audio-volume-medium.svg"
            else
                icon_name="$HOME/.icons/audio-volume-high.svg"
            fi
        fi
    fi
fi
notify-send " " -i $icon_name -c deadbeef_volume -h int:value:$display_volume -h string:synchronous:volume

#bar=$(seq -s "•" $(($display_volume / 5)) | sed 's/[0-9]//g')
#end=$(seq -s " " $(((100 - $display_volume) / 5)) | sed 's/[0-9]//g')
#notify-send " " -i $icon_name -c deadbeef_volume -h string:synchronous:volume "    $bar$end"
