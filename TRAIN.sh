#!/bin/sh

# Disables screensaver and energy-save blanking off of monitor
# and opens fullscreen clock in conky.
# Reenables screensaver on exit.
# I'm using it for home workout.



# set -x
trap cleanup 1 2 3 6 EXIT

cleanup()
{
	echo "Exiting, reverse state."
	xset s on +dpms
	sleep 2
	xautolock -enable
	echo "Done, quitting."
	notify-send -u normal -t 5000 -- 'LOCK on'
	exit 1
}

### main script
xset s off -dpms
xautolock -disable
conky -c $HOME/.Conky/revolutionary_clocks/rev_hd/conkyrc2
