#!/bin/bash

# Wrapper for xdg type and association, with color.

bold=$(tput bold)
red=$(tput setaf 1)
green=$(tput setaf 2)
reset=$(tput sgr0)

TYPE=$(xdg-mime query filetype "$2")
ASS=$(xdg-mime query default "$TYPE")


case $1 in

info)
	echo "Mimetype ${green}$TYPE${reset} opens with ${green}$ASS${reset}"
	exit 1
	;;

*)
	echo "usage: $0" >&2
	exit 1
	;;
esac 
exit
