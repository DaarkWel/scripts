#!/bin/bash

# Parser and opener for geolocation urls,
# parses coordinates and creates gpx file
# and opens it in gpxsee for gpxsee can't
# open just coordinates it needs a gpx file
# therefore the name  :-)

# There's also option for opening in browser,
# with https://nakarte.me

#set -x

LINK=$2
#date in gpx specification format
DATE="$(date +%Y-%m-%dT%H:%M:%SZ)"

NAME="/tmp/${DATE//:/-}.gpx"

# checking and parsing
# there's a lot url formats for yandex maps

if [[ $(echo "$LINK" | awk -F'[/]' '{print $3}') == "yandex.ru" ]]
	then
		# there is some kind of path on yandex maps, we take first point coordinates only
		if echo "$LINK" | grep "maps?rtext" > /dev/null
		then
		LAT=$(echo "$LINK" | awk -F'[=%]' '{print $2}')
		LON=$(echo "$LINK" | awk -F'%2C|~' '{print $2}')
		# for other two formats there's ultimate solution :-)
		elif echo "$LINK" | grep maps/ > /dev/null
		then
		LAT=$(echo "$LINK" | awk -F'?ll=|[&]|[,]|%2C' '{print $3}')
		LON=$(echo "$LINK" | awk -F'?ll=|[&]|[,]|%2C' '{print $2}')
		fi
# now for google maps
elif [[ $(echo "$LINK" | awk -F'[/]' '{print $3}') == "maps.google.com" ]]
	then
	if echo "$LINK" | grep "maps&q=" > /dev/null
	then
		LAT=$(echo "$LINK" | awk -F"[=&,]" '{print $2}')
		LON=$(echo "$LINK" | awk -F"[=&,]" '{print $3}')
	elif echo "$LINK" | grep "maps?f=" > /dev/null
	then
		LAT=$(echo "$LINK" | awk -F"q=|[&]|[,]" '{print $4}')
		LON=$(echo "$LINK" | awk -F"q=|[&]|[,]" '{print $5}')
	fi
else exit 1
fi

case $1 in

gpxsee)

	printf '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>\n'\
'<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="tmp" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd" version="1.1">\n'\
'     <metadata>\n'\
'            <time>'%s'</time>\n' "$DATE"\
> "${NAME}"

	printf '    </metadata>\n'\
'	<trk>\n'\
'		<name></name>\n'\
'		<trkseg>\n'\
>> "${NAME}"

	printf '			<trkpt lat="'%s'" lon="'%s'"><time>'%s'</time></trkpt>\n' "${LAT}" "${LON}" "${DATE}"\
>> "${NAME}"

	printf '			<trkpt lat="'%s'" lon="'%s'"><time>'%s'</time></trkpt>\n'\
'		</trkseg>\n'\
'	</trk>\n'\
'</gpx>' "${LAT}" "${LON}" "${DATE}"\
>> "${NAME}"

	gpxsee "${NAME}"
	rm "${NAME}"

;;

nakarte)

	xdg-open "https://nakarte.me/#m=18/${LAT}/${LON}&l=O&q=${LAT}%C2%B0${LON}%C2%B0&r=${LAT}/${LON}"

;;

esac

exit
