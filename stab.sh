#!/usr/bin/bash

# Calculate stabilization
ffmpeg -y -i "$1" -vf vidstabdetect=stepsize=4:mincontrast=0:result=transforms.trf -f null -

# Stabilize
ffmpeg -y -i "$1" -vf vidstabtransform=smoothing=20:interpol=bicubic,unsharp=5:5:0.8:3:3:0.4 "${1%.*}"-stab.mp4    

# Remove temporary files
rm -f transforms.trf
