#!/bin/bash

# I didn't remember where I got this script but it is not mine I'm almost sure.

# Unlicenced, I guess


# Lauch-hide-show toggle app window
# First parameter is window title, second is app executable name.
# If there's no window with this title then app will launch.
# If there's window with this title but on other desktop then app will be moved on this desktop and make active.
# If there is on this desktop but not active then it will make active.
# If there is on this desktop and is active then it will be minimized.

# Запуск-прятание-показ окна приложения
# Первый параметр - заголовок окна; второй - программа.
# Если окна с таким заголовком нет - приложение запускается.
# если есть, но на другом десктопе - оно переносится на этот и активизируется;
# если есть на этом десктопе, но не активно - активизируется;
# если есть на этом десктопе и активно - минимизируется.
#set -x
APP=$(wmctrl -pl | grep "$1")
read -r APP_ID APP_DESK TAIL <<< "$APP"
echo "$APP_ID"
#echo $APP_DESK

DESK=$(wmctrl -d | grep "[*]")
read -r CUR_DESK TAIL <<< "$DESK"
#echo $CUR_DESK

if [ "$APP_ID" != "" ];
then
    PROG=$(cat /proc/"$(xdotool getwindowpid "$(xdotool getwindowfocus)")"/comm)
    if [ "$APP_DESK" != "$CUR_DESK" ];
    then
        wmctrl -iR "$APP_ID"
    elif [ "$PROG" != "$2" ];
    then
        wmctrl -iR "$APP_ID"
    else
        wmctrl -ir "$APP_ID" -b toggle,hidden
    fi
else
    $2
fi
