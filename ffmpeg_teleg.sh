#!/bin/bash

arr=("$@")
COUNTER=0
for input in "${arr[@]}"
	do
	mapfile -t DIMS < <(mediainfo --Inform="Video;%Width%\n%Height%" "$input")
	FFARG=(-map_metadata -1 -pix_fmt yuv420p -vf scale=592:-2:flags=lanczos -af loudnorm -c:v libx264 -crf 24 -preset veryslow -c:a aac -progress -)
	(( DIMS[0] <= DIMS[1] )) && FFARG[1]="scale=-2:592:flags=lanczos"
	COUNTER=$((++COUNTER))
	echo "# Converting $input"
	(( ${#arr[@]} > 1 )) && INFO1="( $COUNTER of ${#arr[@]} files )"
	PIPE="/tmp/${input##*/}.ffmpegFIFO"
	if [ ! -p "$PIPE" ]; then
		mkfifo "$PIPE"
	fi

	TOTAL=$((1000*$(mediainfo --Inform="General;%Duration%" "$input")))

	ffmpeg -hide_banner -loglevel quiet -i "$input" "${FFARG[@]}" "$input"_teleg.mp4 | awk -F"=" '$0 ~ /out_time_us=[0-9]+$/ {print $2; fflush()}' > "$PIPE" &
PID=$!

while [ -e /proc/$PID ];
do
	read -r CURRENT < "$PIPE"
	if [ -n "$CURRENT" ]
		then
			echo $((200*CURRENT/TOTAL - 100*CURRENT/TOTAL)) # Really right rounding https://stackoverflow.com/a/59414940
		else
			break
	fi
done | zenity --width=400 --height=100 --progress --title="Please wait" --text="Converting $input $INFO1" --auto-close --auto-kill

rm "$PIPE"

done

exit
