#!/bin/sh

# Simple one-liner shows all installed pacman packages by size.

env LC_ALL=C pacman -Qi | gawk '/^Name/ { x = $3 }; /^Installed Size/ { sub(/Installed Size  *:/, ""); gsub (" ", "", $0); print x"\t\t: " $0 }' | LC_ALL=C sort -h -t : -k2
