#!/bin/bash

# jack server reloader.
# some programs, especially games, doesn't work with jack.

# Now works with loopback device, see https://github.com/markc/alsa/blob/master/lib/md/Jack_and_Loopback_device_as_Alsa-to-Jack_bridge.md

# .asound.aloop file content:

# hardware 0,0 : used for ALSA playback
#pcm.loophw00 {
#  type hw
#  card Loopback
#  device 0
#  subdevice 0
#  format S32_LE
#}
#
## playback PCM device: using loopback subdevice 0,0
## Don't use a buffer size that is too small. Some apps 
## won't like it and it will sound crappy 
#
#pcm.amix {
#  type dmix
#  ipc_key 219345
#  slave {
#    pcm loophw00
#    period_size 4096
#    periods 3
#  }
#}
#
#
## for jack alsa_in: looped-back signal at other ends
#pcm.cloop {
#  type hw
#  card Loopback
#  device 1
#  subdevice 0
#  format S32_LE
#}
#
## hardware 0,1 : used for ALSA capture
#pcm.loophw01 {
#  type hw
#  card Loopback
#  device 0
#  subdevice 1
#  format S32_LE
#}
#
## for jack alsa_out: looped-back signal at other end
#pcm.ploop {
#  type hw
#  card Loopback
#  device 1
#  subdevice 1
#  format S32_LE
#}
#
## duplex device combining our PCM devices defined above
#pcm.aduplex {
#  type asym
#  playback.pcm "amix"
#  capture.pcm "loophw01"
#}
#
## default device
#pcm.!default {
#  type plug
#  slave.pcm aduplex
#
#  hint {
#       show on
#       description "Duplex Loopback"
#  }
#}



icon_name=""

if jack_control status &> /dev/null; then
        icon_name=""
        text="JACK Disconnected"
#        :> "$HOME"/.asoundrc
        jack_control stop
                
    else
#        cat "$HOME"/.asoundrc.aloop > "$HOME"/.asoundrc
        $HOME/scripts/jack_start.sh
        text="JACK Connected"
        
fi

notify-send "$text" # -i $icon_name
