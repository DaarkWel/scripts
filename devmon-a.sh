#!/bin/bash

# Indication of manual hot-key mounting with devmon.

devmon -a | zenity --progress --pulsate --auto-close --title="Devmon" --text="Mounting..."
