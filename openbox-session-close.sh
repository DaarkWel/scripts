#!/bin/sh

# Definitely not mine but I added some things...
# Unlicenced, I guess

# Gently closes all windows before reboot or poweroff or X exit.


TIME_TO_WAIT=30
SLEEP=0.25

CLIENTS=""
WINDOWS=$(xprop -root _NET_CLIENT_LIST | awk -F# '{ gsub ("[,]","",$2); print $2}')
for ID in $WINDOWS; do
	WINDOW_TYPE=$(xprop -id "$ID" _NET_WM_WINDOW_TYPE)
	TYPE_NOT_SET=$(echo "$WINDOW_TYPE" | grep -c "not found")
	TYPE_NORMAL=$(echo "$WINDOW_TYPE" | grep -c _NET_WM_WINDOW_TYPE_NORMAL)
	if [ "$TYPE_NOT_SET" -eq 1 ] || [ "$TYPE_NORMAL" -eq 1 ]; then
		CLIENTS="$CLIENTS $ID"
	fi
done

if [ -n "$CLIENTS" ]; then
	for ID in $CLIENTS; do
		wmctrl -ia "$ID"
		sleep $SLEEP
		wmctrl -ic "$ID"

		TIME=$(date +%s)
		while [ "$(date +%s)" -le $((TIME + TIME_TO_WAIT)) ]; do
			sleep $SLEEP
			if [ "$(xprop -root _NET_CLIENT_LIST | grep -c "$ID")" -eq 0 ]; then
				break
			fi
		done
	done
fi

case $1 in
-r)
	history -a && tac ~/.bash_history | awk '!x[$0]++' | tac > ~/.bash_history.tmp && mv -f ~/.bash_history.tmp ~/.bash_history && history -c && history -r
	sudo reboot
	;;
-h)
	tac ~/.bash_history | awk '!x[$0]++' | tac > ~/.bash_history.tmp && mv -f ~/.bash_history.tmp ~/.bash_history
	sudo halt -p
	;;
*)
	openbox --exit
esac
