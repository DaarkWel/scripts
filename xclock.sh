#!/bin/bash

# Semitransparent (with transset-df) xclock launch-kill script.

if pidof xclock; then
	killall xclock &> /dev/null
		else xclock -d -brief -update 1 >& /dev/null & sleep 0.2s && transset-df -n xclock --no-regex
	fi
	
exit
