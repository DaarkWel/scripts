#!/bin/bash

#export LANG=C
#PACMANLOG=/var/log/pacman.log
#LOG=/tmp/pacman_.log

# create empty logfile if non exists
#if [ ! -f ${LOG} ] ; then
#	touch ${LOG}
#fi

# we only want to proceed new entries, old ones are already included in the log
#diff --new-line-format="%L" --old-line-format="" --unchanged-line-format="" ${LOG} ${PACMANLOG} > /tmp/process.log

printf "Please wait...  "
awk '/] installed|] upgraded|] removed/ {gsub("[ ][[].*[]]","");print}' /var/log/pacman.log > /tmp/process.log

# We need GNU awk for switch/case statemant. Luckily in archlinux gawk is dependancy of pacman.
gawk 'BEGIN{

FS="[[]|[]]"

}

{
# We might have two different timedate strings in our log, pre- and post-october 2019.
# Converting them to UNIX format.

if ($2 ~ /T/)

	{
		split ($2, nDate, "[-]|[ ]|T|[:]|[+]");
		datE = mktime(nDate[1]" "nDate[2]" "nDate[3]" "nDate[4]" "nDate[5]" "nDate[6])
	}

else

	{
		split ($2, oDate, "[ ]|[-]|[:]");
		datE = mktime(oDate[1]" "oDate[2]" "oDate[3]" "oDate[4]" "oDate[5]" 00")
	}

# Take state of program to array
match ($0, /installed|upgraded|removed/, stateS)

	{
		split (substr ($0, RSTART), nameProg, " ")
	}

# And make it one-letter mark
if (stateS[0] == "installed")

	{
		finalState="A"
	}

else if (stateS[0] == "upgraded")

	{
		finalState="M"
	}

else if (stateS[0] == "removed")

	{
		finalState="D"
	}

# Some program have dot in their name, it confuses gource, so we replace it w/ underscore
gsub ("[.]","_",nameProg[2])

# Main name categorizing section
switch (nameProg[2])

	{
		case /^librewolf|^firefox|^thunderbird|^spidermonkey|^firedragon|^palemoon|^icecat|^seamonkey|^waterfox/:
			finalnameProg = "mozilla/"nameProg[2]".mozilla|996633"
			break
		case /libreoffice/:
			finalnameProg = "libreoffice/"nameProg[2]".libreoffice|18A303"
			break
		case /^lib.*32.*/:
			finalnameProg = "lib/32/"nameProg[2]".lib|585858"
			break
		case /^lib/:
			finalnameProg = "lib/"nameProg[2]".lib|585858"
			break
		case /^xorg|xf86/:
			finalnameProg = "xorg/"nameProg[2]".xorg|ED541C"
			break
		case /^[ot]tf-/:
			finalnameProg = "font/"nameProg[2]".font|000000"
			break
		case /^perl/:
			finalnameProg = "perl/"nameProg[2]".perl|FF0000"
			break
		case /^haskell/:
			finalnameProg = "haskell/"nameProg[2]".haskell|FF1300"
			break
		case /^python/:
			finalnameProg = "python/"nameProg[2]".python|FF2600"
			break
		case /^php/:
			finalnameProg = "php/"nameProg[2]".php|6C7EB7"
			break
		case /gnome/:nameProg[2]".gnome|5C33170"
			break
		case /^gtk/:
			finalnameProg = "gtk/"nameProg[2]".gtk|FFFF00"
			break
		case /^qt[2-6]-/:
			finalnameProg = "qt/"nameProg[2]".qt|91219E"
			break
		case /^gstreamer|^gst-plugin/:
			finalnameProg = "gstreamer/"nameProg[2]".gstreamer|FFFF66"
			break
		case /^tectonic|^texlive|^latex/:
			finalnameProg = "texlive/"nameProg[2]".texlive|660066"
			break
		default:
			finalnameProg = nameProg[2]
	}

# Output
print datE "|root|" finalState "|" finalnameProg

}'  < /tmp/process.log > /tmp/final &

# Spinner section mostly from https://unix.stackexchange.com/a/225183
PID=$!; i=1; sp="/-\|"
while [ -d /proc/$PID ]
	do
		SPIN=${sp:i++%${#sp}:1}
		printf "\b%s" "$SPIN"
		sleep .1
	done

gawk -F"[|]|[/]" '{
if ($0 ~ /[/]/)

	{
		print $1"|"$2"|"$3"|"$5"|"$6
	}

else

	{
		print $0
	}

}' < /tmp/final > /tmp/final.pie

printf "\r                \nDone\n"
