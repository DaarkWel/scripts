#!/bin/bash

BKUP_DIR="$HOME/.backups"

find "$BKUP_DIR"/QB -mindepth 1 -type f -name '*.zstd' -mtime +2 -print -delete
tar --use-compress-program='zstd --ultra -22 -T0' -cvf "$BKUP_DIR"/QB/qb_"$(date -I)".tar.zstd "$HOME"/.local/share/qBittorrent
