#!/bin/bash

# Quick video transcoder from sertain portable camcorder for bombono dvd authoring.

for f in *MP4 ; do
	[ -e "$f" ] || { echo -e "\e[01;38;05;196m""No input files, abort.""\e[m" ; exit 1; }
	ffmpeg -i "$f" -map_channel 0.1.1 -map_channel 0.1.1 -vf "yadif=0:-1:0, vaguedenoiser, scale=720:480, pad=720:540:0:30:black" -sws_flags lanczos -vcodec ffv1 -acodec pcm_s32le "${f%.*}".avi;

done

exit
