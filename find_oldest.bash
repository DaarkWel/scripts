#!/bin/bash

# Find oldest file in filesystem
# Inspired by the https://superuser.com/a/930013
# and some comments from there

# Unlicenced, I guess

case $1 in

--atime|-a)
	_typedate="%AY-%Am-%Ad %h/%f\n"
	_type="Access"
;;

--mtime|-m)
	_typedate="%TY-%Tm-%Td %h/%f\n"
	_type="Modification"
;;

--ctime|-c)
	_typedate="%CY-%Cm-%Cd %h/%f\n"
	_type="Status Change"
;;

*)
	echo "usage: $0 [--atime | --mtime | --ctime]"
	echo "--atime or -a — print and sort by Access Time"
	echo "--mtime or -m — ... Modification Time"
	echo "--ctime or -c — ... Status Change Time"
	exit 1
;;
esac
# do not descend in /proc
# exclude not readable files
# find all files and show their (A)ccess time (use %T with the first printf for modification time or %C for status change instead)
find . -path "/proc/*" -prune -o ! -readable -prune -o -wholename "*" -type f -printf "$_typedate" | awk -v TYPE="$_type" 'BEGIN {cont=0; oldd=strftime("%Y-%m-%d"); } { if ($1 < oldd) { oldd=$1; oldf=$2; for(i=3; i<=NF; i++) oldf=oldf " " $i; print oldd " " oldf; }; count++; } END { print "\n--------------", "\nSorted by "TYPE" Time", "\nOldest file:", oldd, oldf, "\nTotal compared: ", count}'

exit
