#!/bin/bash

# Helper script for my win guest in qemu.

qemu-system-x86_64 \
-boot order=c \
-drive file=/media/taw3/win.cow \
-m 3G -enable-kvm \
-vga qxl -device virtio-serial-pci \
-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 -chardev spicevmc,id=spicechannel0,name=vdagent -spice unix=on,addr=/tmp/vm_spice.socket,disable-ticketing=on \
-cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time \
-net nic -net user,smb="$HOME"/tmp/share \
-smp 4 \
--daemonize

spicy --uri="spice+unix:///tmp/vm_spice.socket"
