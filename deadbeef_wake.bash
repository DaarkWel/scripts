#!/bin/bash

# Deadbeef as alarm clock, increasing volume.

deadbeef --volume 10
deadbeef --play

for n in $(seq 10 2 70);
 do deadbeef --volume $n;
 sleep 2;
done
