#!/bin/bash

# Big ultimate file and app wrapper for Total Commander under WINE.

#set -x
func_name()
{
# Сдвигаем параметры, чтобы исключить имя кейса как $1
# Shift input parameters to exclude case name as $1
		shift
# Поочерёдно обрабатываем параметры ("$@")
# Handle parameter one by one
		for arg ;
			do
# Набиваем массив сконвертированными именами файлов
# Convert full filenames to linux format and add them to array
			arr+=("$(winepath -u "$arg" | awk -F':/' '{print "/"$2}')")
		done
}

func_image_conv()
{
	[ ! -x "/usr/bin/convert" ] && beep && exit 1
	COUNTER=0
	for input in "${arr[@]}"
		do
		COUNTER=$((++COUNTER))
		echo "# Converting $input"
			if [ ${#arr[@]} -gt 1 ]; then
				INFO1="( $COUNTER of ${#arr[@]} files )"; else
				INFO1=""
			fi
		zenity --width=400 --height=100 --progress --pulsate --title="Converting $input $INFO1" --text="Converting $input $INFO1" --auto-close < <(convert "$input" -filter Lanczos -resize "$PAR1"% $PAR2 "${input%.*}_${PAR1}.${EXT}")
		done
	exit
}

func_name "$@"

case $1 in

musicplayer)
		deadbeef "${arr[@]}"
		exit
;;

ghb)
		ghb "${arr[@]}"
		exit
;;

dc)
		doublecmd "${arr[@]}"
		exit
;;
	
rawtherapee)
		rawtherapee "${arr[@]}"
		exit
;;

gimp)
		gimp "${arr[@]}"
		exit
;;

inkscape)
		inkscape "${arr[@]}"
		exit
;;

geeqie)
		geeqie -f "${arr[@]}"
		exit
;;

mkvtoolnix)
		QT_QPA_PLATFORMTHEME=qt5ct mkvtoolnix-gui "${arr[@]}"
		exit
;;

trimage)
		[[ ! -x "/usr/bin/guake" ]] && beep && exit 1
		guake -ne;
		guake --rename-current-tab="trimage" --execute="/usr/bin/trimage -f \"${arr[0]}\"";
		guake --show
		exit
;;

oxipng)
		[[ ! -x "/usr/bin/guake" ]] && beep && exit 1
		guake -ne;
		guake --rename-current-tab="oxipng" --execute="oxipng -o 3 --strip all -i 0 -b \"${arr[0]}\"";
		guake --show
		exit
;;

coolreader)
		crqt "${arr[0]}";
		exit
;;

im25png)
		PAR1="25"
		PAR2=""
		EXT=png
		func_image_conv
;;

im25jpg)
		PAR1="25"
		PAR2="-quality 85"
		EXT=jpg
		func_image_conv
;;

im50jpg)
		PAR1="50"
		PAR2="-quality 85"
		EXT=jpg
		func_image_conv
;;

im50png)
		PAR1="50"
		PAR2=""
		EXT=png
		func_image_conv
;;

audacity)
		audacity "${arr[@]}"
		exit
;;

geany)
		geany "${arr[@]}"
		exit
;;

ocenaudio)
		ocenaudio "${arr[@]}"
		exit
;;

mediainfo)
		mediainfo-gui "${arr[@]}"
		exit
;;

ffmpeg1)
		"$HOME"/scripts/ffmpeg_teleg.sh "${arr[@]}"
		exit
;;

ffmpeg_lame)
		parallel ffmpeg -i {} -qscale:a 2 {.}.mp3 ::: "${arr[@]}"
		exit
;;

*)
	echo "usage: $0" >&2
	exit 1
;;

esac
