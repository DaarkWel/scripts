#!/bin/sh
#set -x

# Quick-n-dirty udev automounter, with support for password-protected LUKS hot-plug devices.

pathtoname() {
	udevadm info -p /sys/"$1" | awk -v FS== '/DEVNAME/ {print $2}'
}

pathtofstype() {
	udevadm info -p /sys/"$1" | awk -v FS== '/ID_FS_TYPE/ {print $2}'
}

stdbuf -oL -- udevadm monitor --udev -s block | while read -r -- _ _ event devpath _; do
		if [ "$event" = add ]; then
			if [ "$(pathtofstype "$devpath")" = crypto_LUKS ]; then
				devname=$(pathtoname "$devpath")
				udisksctl unlock -b "$devname" --key-file <(zenity --password | tr -d '\n'); else
				devname=$(pathtoname "$devpath")
				udisksctl mount --block-device "$devname" --no-user-interaction
			fi
		fi
done
