#!/usr/bin/env python

import os
from dotenv import load_dotenv
import logging
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler

load_dotenv()
TOKEN = os.getenv('TOKEN')

logging.basicConfig(
	filename='/tmp/nvcmf.log',
	filemode='a',
	format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
	level=logging.INFO
)

ALLOWED = []
ALLOWED.append(int(os.getenv('ALLOWED')))


async def pruntt(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		try:
			ip4file = open('/tmp/ip4_nvcmf', 'r')
		except FileNotFoundError:
			IP4 = "NA"
		else:
			IP4 = ip4file.read()
		import netifaces
		NI = netifaces.ifaddresses('eth0')[netifaces.AF_INET6]
		j = []
		for i in NI:
			j.append(i['addr'])
		IP6 = '\n'.join(j)
		text = f'{IP4}{IP6}'
		MESSAGE = "User " + str(chat_id) + " used this"
		logging.info(MESSAGE)
		await context.bot.send_message(chat_id=update.effective_chat.id, text=text)
	else:
		MESSAGE = "User " + str(chat_id) + " was sent packing"
		logging.info(MESSAGE)
		raise DispatcherHandlerStop


async def deadbeef_status(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		d_status = subprocess.run(['deadbeef', '--nowplaying-tf', '%artist% - %title%'], capture_output=True)
		await context.bot.send_message(chat_id=update.effective_chat.id, text=d_status.stdout.decode('utf-8'))


async def deadbeef_next(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		subprocess.run(['deadbeef', '--next'])


async def deadbeef_prev(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		subprocess.run(['deadbeef', '--prev'])


async def deadbeef_vol_up(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		d_status = subprocess.run(['deadbeef', '--volume', '+5'], capture_output=True)
		await context.bot.send_message(chat_id=update.effective_chat.id, text=d_status.stdout.decode('utf-8'))


async def deadbeef_vol_down(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		d_status = subprocess.run(['deadbeef', '--volume', '-5'], capture_output=True)
		await context.bot.send_message(chat_id=update.effective_chat.id, text=d_status.stdout.decode('utf-8'))


async def deadbeef_vol_mute(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		d_volume_status = subprocess.run(['deadbeef', '--volume'], capture_output=True)
		if int(d_volume_status.stdout.decode('utf-8').split("%")[0]) != 0:
			volfile = open('/tmp/deadbeef_volume_mem', 'w')
			volfile.write(d_volume_status.stdout.decode('utf-8').split("%")[0])
			volfile.close()
			subprocess.run(['deadbeef', '--volume', '0'])
			await context.bot.send_message(chat_id=update.effective_chat.id, text='Muted')
		else:
			volfile = open('/tmp/deadbeef_volume_mem', 'r')
			d_volume = volfile.read()
			volfile.close()
			subprocess.run(['deadbeef', '--volume', d_volume])
			await context.bot.send_message(chat_id=update.effective_chat.id, text='Unmuted')


async def deadbeef_play_pause(update: Update, context: ContextTypes.DEFAULT_TYPE):
	chat_id = update.effective_chat.id
	if chat_id in ALLOWED:
		import subprocess
		subprocess.run(['deadbeef', '--play-pause'])


if __name__ == '__main__':
	application = ApplicationBuilder().token(TOKEN).build()
	application.add_handler(CommandHandler('prunt', pruntt))
	application.add_handler(CommandHandler("dsta", deadbeef_status))
	application.add_handler(CommandHandler("dnext", deadbeef_next))
	application.add_handler(CommandHandler("dprev", deadbeef_prev))
	application.add_handler(CommandHandler("dvolup", deadbeef_vol_up))
	application.add_handler(CommandHandler("dvoldn", deadbeef_vol_down))
	application.add_handler(CommandHandler("dvolmute", deadbeef_vol_mute))
	application.add_handler(CommandHandler("dpp", deadbeef_play_pause))
	application.run_polling()
