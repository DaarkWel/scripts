#!/bin/bash

# Helper script for my artix guest in qemu.

#-cdrom ~/soft/artix-community-qt-openrc-20230814-x86_64.iso \

qemu-system-x86_64 \
-boot order=c \
-drive file="$HOME/VM/artix_pl.cow" \
-m 3G -enable-kvm \
-vga qxl -device virtio-serial-pci \
-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 -chardev spicevmc,id=spicechannel0,name=vdagent -spice unix=on,addr=/tmp/vm_spice2.socket,disable-ticketing=on \
-cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time \
-net nic -net user \
-smp 4 \
--daemonize

spicy --uri="spice+unix:///tmp/vm_spice2.socket"
