#!/bin/bash

# Just a monthly reminder used with cron.

export DISPLAY=":0.0" && notify-send --urgency critical "Water!"
