#!/bin/bash
#set -x
case $1 in

monoff)

	xautolock -locknow

;;

monfulloff)

	sudo ddccontrol dev:/dev/i2c-0 -r 0xd6 -w 4 >& /dev/null
	xautolock -locknow

;;

*)

	echo "usage: $0 monfulloff|monoff" >&2
	exit 1

;;

esac
