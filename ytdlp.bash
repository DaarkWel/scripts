#!/bin/bash

# Wrapper script for yt-dlp (initially youtube-dl) progress bar with zenity.
# And some pre-provided output settings.

#set -x

NAME=$(echo $RANDOM | md5sum | head -c 10)
PIPE="/tmp/ytdlp_$NAME.FIFO"
URL=$2
if [[ ! -p "$PIPE" ]]; then
	mkfifo "$PIPE"
	else
	exit 1
fi

case $1 in

best)	params="--no-mtime --no-playlist -f bv[height<=1080]+ba" ;;
bestpl)	params="--no-mtime -f b[height<=1080]/bv[height<=1080]+ba" ;;
bestaudio)	params="--no-mtime --no-playlist -f bestaudio" ;;
opusaudio)	params="--no-mtime --no-playlist -f bestaudio[acodec=opus]" ;;
-h | --help)	echo "Usage: $0 best | bestaudio | opusaudio"; exit 1 ;;
*)	echo "$(tput setaf 1)Incorrect parameter $(tput bold)«$1»$(tput sgr0). Use $(tput bold)«best»$(tput sgr0), $(tput bold)«bestaudio»$(tput sgr0) or $(tput bold)«opusaudio».$(tput sgr0)" ; exit 2 ;;

esac

yt-dlp --proxy="socks5://127.0.0.1:1080" --newline --print-to-file '%(title)s' "$PIPE" -o "$HOME/Downloads/%(title)s.%(ext)s" --prefer-free-formats --no-colors $params "$2" 2>&1 |\
tee >( grep -oP 'Destination:\ \K.*' >> "$HOME"/.log/youtubedlwr.log && printf "Url=%s" "$URL" >> "$HOME"/.log/youtubedlwr.log && printf "\n___________________________________________\n\n" >> "$HOME"/.log/youtubedlwr.log ) |\
grep --line-buffered -oP '^\[download\].*?\K([0-9.]+\%|#\d+ of \d)' |\
zenity --progress --title="Yt-dlp" --text="Downloading $(cat "$PIPE")"
rm "$PIPE"

unset NAME
unset PIPE
unset URL
exit
