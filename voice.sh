#!/bin/bash

# Hot key clipboard text voice reader with RHVoice

if pidof RHVoice-test > /dev/null
then
	notify-send RHVoice "Текст из буфера уже читается";
else
	notify-send RHVoice "Чтение из буфера обмена" &
	xsel -b | RHVoice-test
fi
