#!/bin/bash

# Script for conky representation of portable devices.


# Find all mounted devices in /media and exclude static one(s)
# Then we parse for name, beautify it (well, with udisks2 we don't need to)
# Then we output conky-specific code for this device, with graphs and bells and whistle :-)

findmnt -nlo TARGET | awk '/\/media\// && !/taw3/ {print}'|\

while read -r line ;

	do sname="$(echo "$line" | cut -d"/" -f3)";
		if (( ${#sname} > 18 ));
			then shortsname="${sname::11}...";
		else shortsname="$sname:";
		fi;

	sdev="$(findmnt -nlo SOURCE "$line")";

	echo '${voffset -4}${color3}${GOTO 29}+${GOTO 39}'"$shortsname"'${color}${GOTO 130}${fs_bar 10,72 '"$line"'} ${color3}${fs_free '"$line"'} of ${fs_size '"$line"'} free${color} ${GOTO 130}${diskiograph '"$sdev"' 10,72 99BBFF 11aaff}';
	done 

exit 0
