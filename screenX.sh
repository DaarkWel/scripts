#!/bin/bash

# Screenshots taker.

shotsdir="$HOME"/tmp/shots

file="$shotsdir/shot-$(date +%Y-%b-%d_%H-%M-%S).png"
#mkdir -p $shotsdir

func_zen_gimp()
{
	if zenity --question --title "Запрос" --text "Редактировать скриншот?";
	then
		gimp "$file";
	fi
}

case $1 in

# Capture a window or rectangle, for window click one, for area select one.
win)
	import png:- | tee >(xclip -selection clipboard -t image/png) > "$file" 
	func_zen_gimp
	;;

# Capture fullscreen

full)
	import -window root png:- | tee >(xclip -selection clipboard -t image/png) > "$file"
	func_zen_gimp
	;;

*)
	echo "usage: $0 win|full" >&2
	exit 1
	;;

esac
exit

