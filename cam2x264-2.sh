#!/bin/bash

# Quick video transcoder from sertain portable camcorder.

#banner()
#{
#  echo "+-------------------------------------+"
#  printf "| %-40s |\n" "$(date)"
#  echo "|                                     |"
#  printf "|$(tput bold) %-35s $(tput sgr0)|\n" "$@"
#  echo "+-------------------------------------+"
#}

#banner "Collecting list"

rm ./mylist.txt > /dev/null 2>&1
for f in ./*.MP4; do
	[ -e "$f" ] || { echo -e "\e[01;38;05;196m""No input files, abort.""\e[m" ; exit 1; }
	echo "file '$f'" >> mylist.txt
done

echo "Done"

#banner "Start encoding"

ffmpeg -hide_banner -loglevel info -f concat -safe 0 -i mylist.txt -map_channel 0.1.1 -map_channel 0.1.1 -vf "yadif=0:-1:0, vaguedenoiser, scale=720:480" -sws_flags lanczos -c:v libx264 -preset fast -crf 18 -c:a aac -q:a 2 output.mkv

rm ./mylist.txt

exit
