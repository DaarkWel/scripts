#!/bin/bash

# Script for conky graph and info of swap usage.

SWAP="$(swapon --show=USED --noheadings --raw --bytes)";
if [[ "$SWAP" == "0" ]]
 then echo ''
  else echo '${GOTO 26}${color3}SWAP:${color}${GOTO 87}${swapbar 10,115} ${swap} of ${swapmax} used'
fi
exit 0
